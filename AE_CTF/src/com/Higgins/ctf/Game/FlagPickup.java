package com.Higgins.ctf.Game;

import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.block.Banner;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;

import com.Higgins.ctf.CTF;

public class FlagPickup implements Listener {
	
	public CTF ctf = new CTF();
	
	@EventHandler
	public void on(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		Block block = e.getClickedBlock();
		
		if(block.getType() == Material.STANDING_BANNER || block.getType() == Material.WALL_BANNER) {
			
			Banner flag = (Banner) block.getState();
			
			Locations loc = new Locations();
			
			if(flag.getBaseColor() == DyeColor.BLUE) {
				
				if(CTF.isOnBlueTeam(p) == true) {
					
					if(block.getLocation() != loc.getBlueFlagSpawn()) {
						
						Bukkit.broadcastMessage(p.getPlayerListName() + " �5has returned the �bBlue Flag!");
						
						block.setType(Material.AIR);
						
						ctf.placeBlueFlag();
					}
				
				} else {
					
					CTF.carrying.add(p);
					ItemStack banner = new ItemStack(Material.BANNER, 1);
					BannerMeta meta = (BannerMeta) banner.getItemMeta();
					meta.setBaseColor(DyeColor.BLUE);
					banner.setItemMeta(meta);
					p.getInventory().setHelmet(banner);
					Bukkit.broadcastMessage(p.getPlayerListName() + " �5has taken the �bBlue Flag!");
					block.setType(Material.AIR);
				}
			}
			
			else if(flag.getBaseColor() == DyeColor.RED) {
				
				if(CTF.isOnRedTeam(p) == true) {
					
					if(block.getLocation() != loc.getRedFlagSpawn()) {
						
						Bukkit.broadcastMessage(p.getPlayerListName() + " �5has returned the �cRed Flag!");
						
						block.setType(Material.AIR);
						
						ctf.placeRedFlag();
					}
				
				} else {
					
					CTF.carrying.add(p);
					ItemStack banner = new ItemStack(Material.BANNER, 1);
					BannerMeta meta = (BannerMeta) banner.getItemMeta();
					meta.setBaseColor(DyeColor.RED);
					banner.setItemMeta(meta);
					p.getInventory().setHelmet(banner);
					Bukkit.broadcastMessage(p.getPlayerListName() + " �5has taken the �cRed Flag!");
					block.setType(Material.AIR);
				}
			}
		}
	}
}
