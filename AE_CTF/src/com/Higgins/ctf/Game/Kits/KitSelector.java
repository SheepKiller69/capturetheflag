package com.Higgins.ctf.Game.Kits;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class KitSelector implements Listener {

	@EventHandler
	public void on(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		Action action = e.getAction();
		
		if(action != Action.PHYSICAL) {
			
			if(p.getInventory().getItemInHand().getType() == Material.EYE_OF_ENDER) {
				
				e.setCancelled(true);
				
				p.openInventory(KitsMain.getInventory());
			}
		}
	}
}

