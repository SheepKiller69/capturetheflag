package com.Higgins.ctf.Game.Kits;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class KitsMain {

	public static Inventory getInventory() {
		Inventory inv = Bukkit.createInventory(null, 9, "Kits");
		craftitem(inv, Material.BOW, 1, "�aArcher", "�7Wooden Axe", "�7Bow", "�732 Arrows");
		craftitem(inv, Material.STONE_SWORD, 1, "�aWarrior", "�7Stone Sword", "�5Chain ChestPlate", null);
		craftitem(inv, Material.WOOD_AXE, 1, "�aScout", "�7Speed 1", "�7Wooden Axe", null);
		craftitem(inv, Material.IRON_SWORD, 1, "�aDefender", "�7Slowness 2", "�7Iron Sword", "�7Iron Chestplate");
		return inv;
	}
	
	public static void craftitem(Inventory inv, Material item, int ammount,
			String displayname, String line1, String line2, String line3) {
		ItemStack onlineppl = new ItemStack(item, ammount);
		ItemMeta onlmeta = onlineppl.getItemMeta();
		onlmeta.setDisplayName(displayname);
		onlmeta.setLore(Arrays.asList(new String[] { line1, line2, line3 }));
		onlineppl.setItemMeta(onlmeta);
		inv.addItem(onlineppl);
	}
}
