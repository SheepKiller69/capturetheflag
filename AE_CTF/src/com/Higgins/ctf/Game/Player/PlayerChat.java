package com.Higgins.ctf.Game.Player;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import com.Higgins.ctf.CTF;

public class PlayerChat implements Listener {

	@EventHandler
	public void on(AsyncPlayerChatEvent e) {
		Player p = e.getPlayer();
		
		if(CTF.isOnRedTeam(p) == true) {
			
			e.setFormat("�c" + "%1$s" + ": �f" + "%2$s");
		}
		
		if(CTF.isOnBlueTeam(p) == true) {
			
			e.setFormat("�b" + "%1$s" + ": �f" + "%2$s");
		}
	}
}
