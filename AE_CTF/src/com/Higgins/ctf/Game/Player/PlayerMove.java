package com.Higgins.ctf.Game.Player;

import net.minecraft.server.v1_8_R3.EnumParticle;
import net.minecraft.server.v1_8_R3.PacketPlayOutWorldParticles;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import com.Higgins.ctf.CTF;

public class PlayerMove implements Listener {

	@EventHandler
	public void on(PlayerMoveEvent e) {
		Player p = e.getPlayer();
		
		if(CTF.carrying.contains(p)) {

			Location loc = p.getLocation();

			for (Player pl : Bukkit.getOnlinePlayers()) {

				((CraftPlayer) pl).getHandle().playerConnection
						.sendPacket(new PacketPlayOutWorldParticles(
								EnumParticle.FLAME, true, (float) loc.getX(),
								(float) loc.getY(), (float) loc.getZ(),
								(float) 0.5, (float) 0.7, (float) 0.7,
								(float) 0, 6));
			}
		}
	}
}
