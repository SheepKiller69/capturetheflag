package com.Higgins.ctf.Game.Player;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;

import com.Higgins.ctf.CTF;
import com.Higgins.ctf.Main;
import com.Higgins.ctf.Titles;
import com.Higgins.ctf.Game.Locations;

public class PlayerRespawn implements Listener {

	@EventHandler
	public void on(PlayerRespawnEvent e) {
		final Player p = e.getPlayer();
		
		CTF ctf = new CTF();
		
		ctf.makeSpectator(p);
		
		Titles.sendTitle(p, "�c�lWait!", 20, 80, 20);
		Titles.sendSubtitle(p, "�eYou'll respawn in 6 seconds!", 20, 80, 20);
		
		p.sendMessage("�c�lWait! �eYou'll respawn in 6 seconds!");
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), new Runnable() {

			@Override
			public void run() {
				
				Locations loc = new Locations();
				
				if(CTF.isOnRedTeam(p) == true) {
					
					p.teleport(loc.getRedSpawn());
				
				} else {
					
					p.teleport(loc.getBlueSpawn());
				}
			}
			
		}, 120L);
	}
}
