package com.Higgins.ctf.Game.Player;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import com.Higgins.ctf.CTF;

public class PlayerDeath implements Listener {

	@EventHandler
	public void on(PlayerDeathEvent e) {
		Player p = e.getEntity();
		
		e.getDrops().clear();
		e.setDroppedExp(0);
		
		//p.spigot().respawn();
		
		e.setDeathMessage(null);
		
		if(p.getKiller() instanceof Player) {
			
			Player k = (Player) p.getKiller();
			
			p.sendMessage("�5You were killed by " + k.getPlayerListName() + "�5 whielding an Iron Sword");
		}
		
		CTF ctf = new CTF();
		
		if(ctf.isCarrying(p) == true) {
			
			ctf.carrying.remove(p);
		}
	}
}
