package com.Higgins.ctf.Game.Player;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.Higgins.ctf.CTF;
import com.Higgins.ctf.GameState;
import com.Higgins.ctf.Main;
import com.Higgins.ctf.Countdowns.LobbyCountdown;
import com.Higgins.ctf.Game.Locations;
import com.Higgins.ctf.Game.TeamManager;
import com.Higgins.ctf.Game.Kits.KitClicks;

public class PlayerJoin implements Listener {

	public TeamManager teams;
	
	@EventHandler
	public void on(PlayerJoinEvent e) {
		Player p = e.getPlayer();
		
		p.getInventory().clear();
		
		p.getInventory().setHelmet(null);
		p.getInventory().setChestplate(null);
		p.getInventory().setLeggings(null);
		p.getInventory().setBoots(null);
		
		p.setHealth(20);
		p.setFoodLevel(20);
		p.setExp(0);
		p.setLevel(0);
		
		if(GameState.getState() == GameState.LOBBY) {
			
			e.setJoinMessage("�5" + p.getName() + " �ehas joined the game �b(" + Bukkit.getOnlinePlayers().size() + "/16)");
			
			teams = new TeamManager();
			
			teams.addRandomTeam(p);
			
			KitClicks.kits.put(p, "archer");
			
			p.getInventory().setItem(4, pickKit());
			p.getInventory().setItem(8, cosmetics());
			
			Locations loc = new Locations();
			
			if(loc.getLobby() != null) {
				
				p.teleport(loc.getLobby());
			}
			
			if(Bukkit.getOnlinePlayers().size() == 2 && !(LobbyCountdown.time > 0)) {
				
				Main.getInstance().startLobbyCountdown();
			}
			
			if(p.getGameMode() == GameMode.SPECTATOR) {
				
				p.setGameMode(GameMode.SURVIVAL);
			}
		
		} else {
			
			e.setJoinMessage(null);
			
			CTF ctf = new CTF();
			
			ctf.makeSpectator(p);
		}
	}

	private ItemStack cosmetics() {
		ItemStack cos = new ItemStack(Material.DIAMOND_HELMET);
		ItemMeta meta = cos.getItemMeta();
		meta.setDisplayName("�aCosmetics");
		cos.setItemMeta(meta);
		return cos;
	}

	private ItemStack pickKit() {
		ItemStack pk = new ItemStack(Material.EYE_OF_ENDER);
		ItemMeta meta = pk.getItemMeta();
		meta.setDisplayName("�aPick a kit");
		pk.setItemMeta(meta);
		return pk;
	}
}
