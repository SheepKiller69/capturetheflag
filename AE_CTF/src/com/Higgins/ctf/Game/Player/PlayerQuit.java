package com.Higgins.ctf.Game.Player;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import com.Higgins.ctf.CTF;
import com.Higgins.ctf.GameState;

public class PlayerQuit implements Listener {

	@EventHandler
	public void on(PlayerQuitEvent e) {
		Player p = e.getPlayer();
		
		CTF.red.remove(p);
		CTF.blue.remove(p);
		
		if(GameState.getState() == GameState.LOBBY) {
			
			int online = Bukkit.getOnlinePlayers().size();
			
			online--;
			
			e.setQuitMessage("�5" + p.getName() + " �ehas left the game �b(" + online + "/16)");
		}
		
		else if(GameState.getState() == GameState.IN_GAME || GameState.getState() == GameState.RESTART) {
			
			e.setQuitMessage(p.getPlayerListName() + " �7has left the game");
			
			CTF ctf = new CTF();
			ctf.carrying.remove(p);
			ctf.spectator.remove(p);
		}
	}
}
