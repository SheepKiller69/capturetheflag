package com.Higgins.ctf;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.Higgins.ctf.Countdowns.LobbyCountdown;
import com.Higgins.ctf.Countdowns.RestartCountdown;
import com.Higgins.ctf.Game.Cosmetics;
import com.Higgins.ctf.Game.FlagCapture;
import com.Higgins.ctf.Game.FlagPickup;
import com.Higgins.ctf.Game.GameUtils;
import com.Higgins.ctf.Game.Kits.KitClicks;
import com.Higgins.ctf.Game.Kits.KitSelector;
import com.Higgins.ctf.Game.Player.PlayerChat;
import com.Higgins.ctf.Game.Player.PlayerDeath;
import com.Higgins.ctf.Game.Player.PlayerJoin;
import com.Higgins.ctf.Game.Player.PlayerMove;
import com.Higgins.ctf.Game.Player.PlayerQuit;
import com.Higgins.ctf.Game.Player.PlayerRespawn;

public class Main extends JavaPlugin {

	public static String prefix = "�5CTF �r";
	
	public static Main instance;
	
    public static int lobby;
    public static int game;
    public static int restart;
	
	@Override
	public void onEnable() {
		
		getConfig().options().copyDefaults(true);
		
		instance = this;
		
		PluginManager pm = Bukkit.getServer().getPluginManager();
		
		pm.registerEvents(new PlayerJoin(), this);
		pm.registerEvents(new PlayerQuit(), this);
		pm.registerEvents(new PlayerDeath(), this);
		pm.registerEvents(new PlayerRespawn(), this);
		pm.registerEvents(new PlayerMove(), this);
		pm.registerEvents(new PlayerChat(), this);
		pm.registerEvents(new GameUtils(), this);
		pm.registerEvents(new FlagPickup(), this);
		pm.registerEvents(new FlagCapture(), this);
		pm.registerEvents(new KitSelector(), this);
		pm.registerEvents(new KitClicks(), this);
		pm.registerEvents(new Cosmetics(), this);
		
		this.getCommand("ctf").setExecutor(new Commands());
		
		GameState.setState(GameState.LOBBY);
	}
	
	public static Main getInstance() {
		return instance;
	}
	
	public void startLobbyCountdown() {
		LobbyCountdown.time = 60;
		lobby = getServer().getScheduler().scheduleSyncRepeatingTask(this, new LobbyCountdown(), 20l, 20l);
	}
	
	public void stopLobbyCountdown() {
		getServer().getScheduler().cancelTask(lobby);
	}
	
	public void startRestartCountdown() {
		RestartCountdown.time = 5;
		restart = getServer().getScheduler().scheduleSyncRepeatingTask(this, new RestartCountdown(), 20l, 20l);
	}
}
