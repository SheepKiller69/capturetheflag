package com.Higgins.ctf;

public enum GameState {

	LOBBY, IN_GAME, RESTART;
	
	public static GameState state;
	
	public static GameState getState() {
		return state;
	}
	
	public static void setState(GameState state) {
		GameState.state = state;
	}
}
