package com.Higgins.ctf.Countdowns;

import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Banner;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_8_R3.block.CraftBanner;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import com.Higgins.ctf.CTF;
import com.Higgins.ctf.GameState;
import com.Higgins.ctf.Main;
import com.Higgins.ctf.Titles;
import com.Higgins.ctf.Game.Locations;
import com.Higgins.ctf.Game.Kits.KitClicks;

public class LobbyCountdown extends BukkitRunnable {

	public static int time;
	
	@Override
	public void run() {
		if(time == 0) {
			if(Bukkit.getOnlinePlayers().size() == 0) {
				
				Bukkit.broadcastMessage(Main.prefix + "Waiting for more players. Restarting countdown...");
				Main.getInstance().stopLobbyCountdown();
				Main.getInstance().startLobbyCountdown();
				return;
			}
			
			GameState.setState(GameState.IN_GAME);
			Main.getInstance().stopLobbyCountdown();
			
			Locations loc = new Locations();
			CTF ctf = new CTF();
			
			ctf.placeRedFlag();
			ctf.placeBlueFlag();
			
			for(Player red : CTF.red) {
				
				red.teleport(loc.getRedSpawn());
				CTF.giveContents(red);
			}
			
			for(Player blue : CTF.blue) {
				
				blue.teleport(loc.getBlueSpawn());
				CTF.giveContents(blue);
			}
			
			for(Player pl : Bukkit.getOnlinePlayers()) {
				
				pl.getInventory().clear();
				
				if(KitClicks.kits.get(pl) == "archer") {
					
					pl.getInventory().addItem(new ItemStack(Material.WOOD_AXE));
					pl.getInventory().addItem(new ItemStack(Material.BOW));
					pl.getInventory().addItem(new ItemStack(Material.ARROW, 32));
				}
				
				if(KitClicks.kits.get(pl) == "warrior") {
					
					pl.getInventory().addItem(new ItemStack(Material.STONE_SWORD));
					pl.getInventory().setChestplate(new ItemStack(Material.CHAINMAIL_CHESTPLATE));
				}
				
				if(KitClicks.kits.get(pl) == "scout") {
					
					pl.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 0));
					pl.getInventory().addItem(new ItemStack(Material.WOOD_AXE));
				}
				
				if(KitClicks.kits.get(pl) == "defender") {
					
					pl.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, Integer.MAX_VALUE, 1));
					pl.getInventory().addItem(new ItemStack(Material.IRON_SWORD));
					pl.getInventory().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
				}
				
			}
			
			return;
		}
		
		if(time % 10 == 0 || time <= 5) {
			
			Bukkit.broadcastMessage("�eStarting in �c" + String.valueOf(time) + " �eseconds.");
			for(Player pl : Bukkit.getOnlinePlayers()) {
				
				Titles.sendTitle(pl, "�eStarting in", 10, 40, 10);
				Titles.sendSubtitle(pl, "�c" + String.valueOf(time) + " �eseconds", 10, 40, 10);
				
				pl.playSound(pl.getLocation(), Sound.NOTE_PLING , 1, 1);
				
			}
		}
		
		time -= 1;
	}
}
