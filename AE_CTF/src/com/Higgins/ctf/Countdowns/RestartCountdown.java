package com.Higgins.ctf.Countdowns;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

public class RestartCountdown extends BukkitRunnable {

	public static int time;

	@Override
	public void run() {
		if(time == 0) {
			
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "restart");
			return;
		}
		time -= 1;
	}
}

