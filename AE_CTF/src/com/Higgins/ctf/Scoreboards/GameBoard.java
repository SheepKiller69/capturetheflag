package com.Higgins.ctf.Scoreboards;

import org.bukkit.Bukkit;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;
import org.bukkit.scoreboard.Team;

public class GameBoard {

	public static ScoreboardManager manager = Bukkit.getScoreboardManager();
    public static Scoreboard board = manager.getNewScoreboard();
    public static Team red = board.registerNewTeam("red");
    public static Team blue = board.registerNewTeam("blue");
    
    public static void makeGameBoard() {
    	
    	red.setPrefix("�c");
    	blue.setPrefix("�b");
    }
}
